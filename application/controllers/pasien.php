<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pasien extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct(){
		parent::__construct();
		$hak_akses = array(0,1);
		no_access($hak_akses);
	}

	
	public function index()
	{
		$this->load->model('pasien_m');
		$data = $this->pasien_m->config_datatable_search();

		$this->load->view('part/header');
		$this->load->view('pasien/pasien', $data);
		$this->load->view('part/footer');
	}

	public function datatable_search()
    {
        $this->load->model('pasien_m');
        $outp = $this->pasien_m->datatable_search($_POST);
        // $this->output
        //         ->set_status_header(200)
        //         ->set_content_type('application/json', 'utf-8')
        //         ->set_output(json_encode($outp, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
        //         ->_display();
		// exit;
		print_r(json_encode($outp));
		
	}


	public function get_single()
	{
		$this->load->model('pasien_m');
        $outp = $this->pasien_m->get_single($_POST['id']);

		print_r(json_encode($outp));
	}

	public function tambah()
	{
		$config = array(
			'form_url' => site_url('pasien/insert'),
			'form_title' =>'tambah data pasien'
		);
		$this->load->view('part/header');
		$this->load->view('pasien/form', $config);
		$this->load->view('part/footer');
	}

	public function insert()
	{
		$this->load->model('pasien_m');
		$outp = $this->pasien_m->insert($_POST);
	
		print_r(json_encode($outp));
		
	}

	public function daftar()
	{
		$this->load->model('pasien_m');
		$data = $this->pasien_m->config_datatable();

		$this->load->view('part/header');
		$this->load->view('pasien/daftar-pasien', $data);
		$this->load->view('part/footer');
	}

	
	public function datatable()
    {
        $this->load->model('pasien_m');
        $outp = $this->pasien_m->datatable($_POST);
	   
		print_r(json_encode($outp));
	}

	public function edit($id = NULL)
	{
		$this->load->model('pasien_m');
	
		$config = array(
			'form_url' => site_url('pasien/update/'.$id),
			'data' => $this->pasien_m->get_single($id),
			'form_title' =>'edit data pasien'
		);


		$this->load->view('part/header');		
		$this->load->view('pasien/form', $config);
		$this->load->view('part/footer');
	}

	public function update($id = NULL)
	{
		$this->load->model('pasien_m');
		$outp = $this->pasien_m->update($id, $_POST);
	
		print_r(json_encode($outp));
				
	}
	
	public function delete()
	{
		$this->load->model('pasien_m');
		$outp = $this->pasien_m->delete($_POST['id_pasien']);

		print_r(json_encode($outp));
		
	}
}
