<style>
	.wajib:after {
		content: " *";
		color:red;
	}
</style>
<!-- konten -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<!-- <section class="content-header">
		<h1>
			Pendaftaran
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Pendaftaran</li>
		</ol>
	</section> -->

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-8">
				<div class="box box-default">
					<div class="box-header with-border">
						<h3 class="box-title"><?php echo ucfirst($form_title) ?></h3>
					</div>
					<form id="form" action="<?php echo $form_url ?>" method="POST">
						<!-- /.box-header -->
					<div class="box-body">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="wajib">Nama Pasien</label>
									<input class="form-control" id="nama_pasien" style="width: 100%;" name="nama_pasien" value="<?php echo isset($data['nama_pasien']) ? $data['nama_pasien'] : '' ?>">
										
								</div>
							</div>
							<div class="col-md-5">
								<div class="form-group">
									<label class="wajib">Nama Ibu</label>
									<input class="form-control" id="nama_ibu" style="width: 100%;" name="nama_ibu" value="<?php echo isset($data['nama_ibu']) ? $data['nama_ibu'] : '' ?>">
								
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="wajib">Tempat Lahir</label>
									<input class="form-control" style="width: 100%;" id="tempat_lahir" name="tempat_lahir" value="<?php echo isset($data['tempat_lahir']) ? $data['tempat_lahir'] : '' ?>">
								
								</div>
							</div>
							<div class="col-md-5">
								<div class="form-group">
									<label for="tgl_lahir" class="wajib">Tanggal Lahir</label>
									<input type="date" class="form-control" style="width: 100%;" id="tgl_lahir" name="tgl_lahir" value="<?php echo isset($data['tgl_lahir']) ? $data['tgl_lahir'] : '' ?>">
									
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label>Alamat</label>
										<textarea class="form-control" id="alamat" name="alamat" rows="4" style="width: 100%;"><?php echo isset($data['alamat']) ? $data['alamat'] : '' ?></textarea>
								</div>
							</div>
						</div>
					
						<br><br>
						<div class="box-footer">
							<div class="pull-right">
								<button type="submit" class="btn btn-success button-sm btn-flat"><i class="fa fa-save"></i> Simpan</button>
							</div>
						</div>
					</form>					
					</div>
				</div>
			</div>
	</section>
	<!-- /.content -->
	
</div>
<!-- /.content-wrapper -->

		


<script>
	$(document).ready(function() {
		//Initialize Select2 Elements
		$('.select2').select2();

		//Date picker
		$('.datepicker').datepicker({
			autoclose: true
		});

		$('form#form').bootstrapValidator({
//      live: 'disabled',
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
			nama_pasien: {
                validators: {
                    notEmpty: {
                        message: 'Nama Pasien tidak boleh kosong'
                    }
                }
			},
			nama_ibu: {
                validators: {
                    notEmpty: {
                        message: 'Nama Ibu tidak boleh kosong'
                    }
                }
			},
			tempat_lahir: {
                validators: {
                    notEmpty: {
                        message: 'Tempat Lahir tidak boleh kosong'
                    }
                }
			},
			tgl_lahir: {
                validators: {
                    notEmpty: {
                        message: 'Tanggal Lahir tidak boleh kosong'
                    }
                }
			},
			
        }
	})
	.on('success.form.bv', function(e) {
            // Prevent form submission
            e.preventDefault();			
			var myform = $('form#form');
			var btnSubmit = myform.find("[type='submit']");
			var btnSubmitHtml = btnSubmit.html();
			var url = myform.attr("action");
			var data = new FormData(form);
			$.ajax({
				beforeSend:function() { 
					btnSubmit.addClass("disabled").html("<i class='fa fa-spinner fa-pulse fa-fw'></i> Loading ... ").prop("disabled","disabled");
				},
				cache: false,
				processData: false,
				contentType: false,
				type: "POST",
				url : url,
				data : data,
				dataType:'JSON',
				success:function(response) {
					btnSubmit.removeClass("disabled").html(btnSubmitHtml).removeAttr("disabled");
					if (response.status == "success" ){
						toastr.success(response.message,'Success !',{ 
							closeButton: true,
							progressBar: true, 
							timeOut: 1000,
							positionClass: "toast-top-right"
						});
						setTimeout(function(){
							if(response.redirect == "" || response.redirect == "reload"){
								location.reload();
							} else {
								location.href = response.redirect;
							}
						},1000);
					} else {
						toastr.error(response.message,'Failed !',{closeButton: true,positionClass: "toast-top-center"});
					}
				}
			});
    	});


	});

</script>
