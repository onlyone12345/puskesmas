<!-- konten -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<!-- <section class="content-header">
		<h1>
			Pendaftaran
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Pendaftaran</li>
		</ol>
	</section> -->

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-8">
				<div class="box box-default">
					<div class="box-header with-border">
						<h3 class="box-title">Data Pasien</h3>


					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label>ID Peserta</label>
									<div class="input-group">
										<input class="form-control" style="width: 100%;" id="id_pasien" disabled>
											
										<span class="input-group-btn"><button class="btn btn-success btn-flat" data-toggle="modal" data-target="#modal-cari-data"><i class="fa fa-search"></i></button></span>		
									</div>
								</div>
							</div>
							
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label>Nama</label>
									<input class="form-control" id="nama_pasien" style="width: 100%;" disabled>
										
								</div>
							</div>
							<div class="col-md-5">
								<div class="form-group">
									<label>Nama Ibu</label>
									<input class="form-control" id="nama_ibu" style="width: 100%;" disabled>
								
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label>Tempat, Tanggal Lahir</label>
									<input class="form-control" style="width: 100%;" id="ttl" disabled>
									
								</div>
							</div>
							<div class="col-md-5">
								<div class="form-group">
									<label>Usia</label>
									<input class="form-control" style="width: 100%;" id="usia" disabled>
									
								</div>
							</div>
							
						</div>

						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label>Alamat</label>
										<textarea class="form-control" id="alamat" rows="4" style="width: 100%;" disabled></textarea>
								</div>
							</div>
						</div>
					
						<br><br>
						<!-- <div class="box-footer">
							<div class="pull-right">
								<a href="<?php echo site_url() ?>data/tambah" type="button" class="btn btn-success button-sm btn-flat"><i class="fa fa-plus"></i> Tambah Data</a>
						
							</div>
						</div> -->
					</div>
				</div>
			</div>
	</section>
	<!-- /.content -->
		<div class="modal fade modal" id="modal-cari-data">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Cari Data Pasien</h4>
              </div>
              <div class="modal-body">
												
				<table id="datatable_custom" class="table table-striped table-bordered" style="width:100%">
					<thead>
						<?php echo $datatable_header ?>
					</thead>
					<tbody>
						
					</tbody>
				</table>
		
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->



</div>
<!-- /.content-wrapper -->

		


<script>
	$(document).ready(function() {
		//Initialize Select2 Elements
		$('.select2').select2();

		//Date picker
		$('.datepicker').datepicker({
			autoclose: true
		});

		var iTable = $('#datatable_custom').DataTable({
			processing: true,
			serverSide: true,
			ajax: {
				url  : "<?php echo site_url($datatable_url) ?>",
				type : "POST",
				error: function(){
					$('.dataTable').find('tbody').html('<tr><td colspan="<?php echo sizeof($datatable_column);?>" style="text-align:center;">No data available in table</td></tr>');
					$(".dataTables_processing").css("display","none");
				},
				data : function ( d ) {
					var obj = {};
					$('[name^=filter]').each(function(){
						var key = $(this).attr('name');
						var val = $(this).val();
						obj[key] = val;
					});
					return $.extend( {}, d, obj);
					}
				},
				columns: [<?php echo implode('',$datatable_column) ?>],
				
			});

		$('#datatable_custom tbody').on('click', '.pilih_pasien', function(){
			var data_id = $(this).attr('data-id');			
			$.ajax({
				url  : "<?php echo site_url('pasien/get_single') ?>",
				type : "POST",
				data : {id:data_id},
				success : function (response) {
					var response = JSON.parse(response);
					$('#id_pasien').val(response.id_pasien);
					$('#nama_pasien').val(response.nama_pasien);
					$('#nama_ibu').val(response.nama_ibu);
					$('#ttl').val(response.tempat_lahir+', '+response.tgl_lahir);
					$('#usia').val(response.usia);					
					$('#alamat').val(response.alamat);
				}
			});
			$('#modal-cari-data').modal('toggle');
		});

		
	});

</script>
