<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pasien_m extends CI_Model {

    public function config_datatable()
	{       
        $data['datatable_url'] = 'pasien/datatable';
        $data['delete_url'] = site_url('pasien/delete');        
        $data['datatable_header'] = '<tr>
                                        <th>ID Pasien</th>
										<th>Nama</th>
										<th>Nama Ibu</th>
                                        <th>Tempat/Tgl Lahir</th>
										<th>Usia</th>                                        
										<th>Alamat</th>
										<th width="50px">Action</th>
									</tr>';
		$data['datatable_column'] = array(
			'{"data": "id"},',
			'{"data": "nama"},',
			'{"data": "nama_ibu"},',
            '{"data": "ttl"},',
			'{"data": "usia"},',            
			'{"data": "alamat"},',
			'{"data": "aksi", "className":"dt-center"},',
		);

		return $data;
	}

	public function datatable($post)
    {   
        
        $total_data = $this->db->get('pasien')->num_rows();
		// $total_filtered = $this->db->get('pasien')->num_rows();
        $column = array('id_pasien', 'nama_pasien', 'nama_ibu', 'tempat_lahir', 'tgl_lahir', 'alamat', 'id_pasien');

        // having
        if(isset($post["search"]["value"]) && !empty($post["search"]["value"]) ) {
			$q		= $post["search"]["value"];
                     
            $this->db->having("id_pasien LIKE '%".$q."%'");
            $this->db->or_having("nama_pasien LIKE '%".$q."%'");
            $this->db->or_having("nama_ibu LIKE '%".$q."%'");
            $this->db->or_having("tempat_lahir LIKE '%".$q."%'");
            $this->db->or_having("tgl_lahir LIKE '%".$q."%'");
            $this->db->or_having("alamat LIKE '%".$q."%'");
        }

        $total_filtered = $this->db->get('pasien')->num_rows();


            // having
        if(isset($post["search"]["value"]) && !empty($post["search"]["value"]) ) {
            $q		= $post["search"]["value"];
                        
            $this->db->having("id_pasien LIKE '%".$q."%'");
            $this->db->or_having("nama_pasien LIKE '%".$q."%'");
            $this->db->or_having("nama_ibu LIKE '%".$q."%'");
            $this->db->or_having("tempat_lahir LIKE '%".$q."%'");
            $this->db->or_having("tgl_lahir LIKE '%".$q."%'");
            $this->db->or_having("alamat LIKE '%".$q."%'");
        }
        // order
        $this->db->order_by($column[$post['order'][0]['column']], $post['order'][0]['dir']);

        // limit
        $this->db->limit($post['length'], $post['start']);
        
        $data = $this->db->get('pasien');
        
        $column = array();
        foreach ($data->result() as $row) {
            $date = date_create($row->tgl_lahir);
			$gg['id'] = $row->id_pasien;
			$gg['nama'] = $row->nama_pasien;
			$gg['nama_ibu'] = $row->nama_ibu;
            $gg['ttl'] = $row->tempat_lahir.", ".date_format($date, "d/m/Y");
			$gg['usia'] = get_usia($row->tgl_lahir);            
			$gg['alamat'] = $row->alamat;
            $gg['aksi'] = '<div class="btn-group"> <a class="btn btn-primary btn-sm btn-flat" href="'.site_url().'pasien/edit/'.$row->id_pasien.'"><i class="fa fa-pencil"></i></a> <a class="btn btn-danger btn-sm btn-flat delete" data-id="'.$row->id_pasien.'"><i class="fa fa-trash"></i></a> </div> ';
            $column[] = $gg;
        }

        $outp = array(
            'draw' => $post['draw'],
            "recordsTotal" => $total_data,
            "recordsFiltered" => $total_filtered,
            "data" => $column,
        );        

        return $outp;
    }

    public function config_datatable_search()
	{       
		$data['datatable_url'] = 'pasien/datatable-search';
        $data['datatable_header'] = '<tr>
                                        <th>ID Pasien</th>
										<th>Nama</th>
										<th>Nama Ibu</th>
										<th>Tempat/Tgl Lahir</th>
										<th>Alamat</th>
										<th width="110px">Action</th>
									</tr>';
		$data['datatable_column'] = array(
			'{"data": "id"},',
			'{"data": "nama"},',
			'{"data": "nama_ibu"},',
			'{"data": "ttl"},',
			'{"data": "alamat"},',
			'{"data": "aksi", "className":"dt-center"},',
		);

		return $data;
	}

	public function datatable_search($post)
    {   
        
        $total_data = $this->db->get('pasien')->num_rows();
        
        // having
        if(isset($post["search"]["value"]) && !empty($post["search"]["value"]) ) {
            $q		= $post["search"]["value"];
                        
            $this->db->having("id_pasien LIKE '%".$q."%'");
            $this->db->or_having("nama_pasien LIKE '%".$q."%'");
            $this->db->or_having("nama_ibu LIKE '%".$q."%'");
            $this->db->or_having("tempat_lahir LIKE '%".$q."%'");
            $this->db->or_having("tgl_lahir LIKE '%".$q."%'");
            $this->db->or_having("alamat LIKE '%".$q."%'");
        }
        $total_filtered = $this->db->get('pasien')->num_rows();
        

        $column = array('id_pasien', 'nama_pasien', 'nama_ibu', 'tgl_lahir', 'alamat', 'id_pasien');
        
        // having
        if(isset($post["search"]["value"]) && !empty($post["search"]["value"]) ) {
			$q		= $post["search"]["value"];
                     
            $this->db->having("id_pasien LIKE '%".$q."%'");
            $this->db->or_having("nama_pasien LIKE '%".$q."%'");
            $this->db->or_having("nama_ibu LIKE '%".$q."%'");
            $this->db->or_having("tempat_lahir LIKE '%".$q."%'");
            $this->db->or_having("tgl_lahir LIKE '%".$q."%'");
            $this->db->or_having("alamat LIKE '%".$q."%'");
        }

        // order
        $this->db->order_by($column[$post['order'][0]['column']], $post['order'][0]['dir']);

        // limit
        $this->db->limit($post['length'], $post['start']);

        $data = $this->db->get('pasien');
        $column = array();
        foreach ($data->result() as $row) {
            $date = date_create($row->tgl_lahir);
			$gg['id'] = $row->id_pasien;
			$gg['nama'] = $row->nama_pasien;
			$gg['nama_ibu'] = $row->nama_ibu;
			$gg['ttl'] = $row->tempat_lahir.", ".date_format($date, "d/m/Y");
			$gg['alamat'] = $row->alamat;
            $gg['aksi'] = '<a class="btn btn-success btn-sm pilih_pasien btn-flat" data-id="'.$row->id_pasien.'"> <i class="fa fa-check"></i></a>';
            $column[] = $gg;
        }

        $outp = array(
            'draw' => $post['draw'],
            "recordsTotal" => $total_data,
            "recordsFiltered" => $total_filtered,
            "data" => $column,
        );        

        return $outp;
    }
    
    public function get_single($id)
    {

        $data = $this->db->where('id_pasien',$id)->get('pasien');
        $column = array();
        foreach ($data->result() as $row) {
            $date = date_create($row->tgl_lahir);
            $cur_date = date_create();
			$gg['id_pasien'] = $row->id_pasien;
			$gg['nama_pasien'] = $row->nama_pasien;
			$gg['nama_ibu'] = $row->nama_ibu;
            $gg['tempat_lahir'] = $row->tempat_lahir;
            $gg['tgl_lahir'] = $row->tgl_lahir;
            $gg['usia'] = get_usia($row->tgl_lahir);
			$gg['alamat'] = $row->alamat;
            $column[] = $gg;
        }

        return $column[0];
    }

    public function insert($post)
    {
        $response['status'] = "fail";
        $response['message'] = "Data can't be save";

        $max = $this->db->select_max('id_pasien')->get('pasien');
        $idMax = (int) $max->result()[0]->id_pasien;
        $post['id_pasien'] = sprintf("%010s", $idMax+1);

		$insert =  $this->db->insert('pasien', $post);
		if($insert){
            $response['status'] = "success";
            $response['message'] = "Data has been save";
            $response['redirect'] = site_url('pasien/daftar');

		}
		return $response;
    }

    public function update($id, $data)
    {
        $response['status'] = "fail";
        $response['message'] = "Data can't be save";

        $this->db->where('id_pasien', $id);
		if ($this->db->update('pasien', $data)) {
            $response['status'] = "success";
            $response['message'] = "Data has been save";
            $response['redirect'] = site_url('pasien/daftar');
        };
        return $response;
    }

     public function delete($id)
    {
        $response['status'] = "fail";
        $response['message'] = "Data can't be save";

        $this->db->where('id_pasien', $id);
		if ($this->db->delete('pasien')) {
            $response['status'] = "success";
            $response['message'] = "Data has been save";
            $response['redirect'] = site_url('pasien/daftar');
        };
        return $response;
    }
    
}